name := "qinu"
 
version := "0.0.1"
 
scalaVersion := "2.10.2"
 
libraryDependencies ++= Seq(
"org.apache.httpcomponents" % "httpmime" % "4.2"
,"org.apache.httpcomponents" % "httpclient" % "4.2.1"
,"junit" % "junit" % "4.9"
)